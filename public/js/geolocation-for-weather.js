//save this info to a cookie for caching

//checking for browser compability
function geolocationData() {

  var geocoder = new google.maps.Geocoder();

  if (navigator.geolocation) {
    var currentPosition = navigator.geolocation.getCurrentPosition(success);
  } else {
    alert("Your browser could not fetch your location"); //error messages should be lyrics
  }
  return currentPosition;

};

function success(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    codeLatLng(lat, lng);
}

function initialize() {
  var geocoder = new google.maps.Geocoder();
}

function codeLatLng(lat, lng) {

  var latlng = new google.maps.LatLng(lat, lng);
  var geocoder = new google.maps.Geocoder();

  geocoder.geocode({'latLng': latlng}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
    // console.log(results);
      if (results) {
          let geoCity = results[0].address_components[3].long_name; //city i.e. Brooklyn
          let sublocality = geoCity.toLowerCase();

          var url = "http://api.openweathermap.org/data/2.5/weather?q=" + sublocality + "&appid=ad4a230f68b83cc0f19b57d82e795048";
          // var url = "/weather?zip=11205"
          var ourRequest = new XMLHttpRequest();
          ourRequest.open('GET', url, true);
          ourRequest.send( );
          ourRequest.onreadystatechange = function () {
            if (ourRequest.readyState === 4 && ourRequest.status === 200) {
              let json = ourRequest.responseText;
              let data = JSON.parse(json);
              // console.log(data);
              let tempK = data.main.temp;
              let temperatureF = (tempK - 273.15) * 1.8000 + 32.00;
              let weather = data.weather[0].main;
              let todaysDate = new Date();

              $("#city").html(geoCity);
              $('#state').html(results[5].address_components[0].long_name);
              $("#temp").html(Math.floor(temperatureF) + "º");
              $("#weather").html(weather);
              $("#date").html(todaysDate.toLocaleString("en", { month: "long"  }) + " " + todaysDate.toLocaleString("en", { day: "numeric" }));
            }
          }
      } else {
        console.log("No results found");
      }

    } else {
     console.log("Geocoder failed due to: " + status);
    }
  });
}

function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "User denied the request for Geolocation."
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred."
            break;
    }
}
