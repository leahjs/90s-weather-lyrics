# Project Name

A weather app that provides a hiphop fact of that day in the 1990s. In addition to the hiphop fact there will be a lyric from the artist the fact is about.

## Installation

Fork [90s Weather Lyrics](https://bitbucket.org/emarkland00/90s-weather-lyrics/ "Fork this Repo") and clone into a directory. `cd` into said directory and run `npm start`.

## Usage

Visit the app daily to get the weather, a hiphop fact and lyric.

## Versions

1. Node.js v5.7.0 stable
2. npm 3.6.0
3. Express Framework ~4.13.1

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request. Thanks!

### Credits

[Errol Markland](https://bitbucket.org/emarkland00/ "Errol Markland") && [Leah Suter](https://bitbucket.org/leahjs/ "Leah Suter")
