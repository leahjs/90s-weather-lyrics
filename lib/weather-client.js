/**
    Wrapper used to interact with a third party weather API
*/
const http = require('http');

function WeatherClient(opts) {
    this.defaults = {
        base_url: "api.openweathermap.org",
        base_path: '/data/2.5',
        protocol: 'http',
        contentType: 'application/json',
        key: 'ad4a230f68b83cc0f19b57d82e795048'
    };
}

/**
 *  Get current weather
 *  @param {json} opt - The options passed to get the current weather
 */
WeatherClient.prototype.getCurrentWeather = function(opt, callback) {
    //TODO: Do better error throwing
    if (!opt) {
        throw new Error("Must specify parameter for fetching current weather");
    };

    var queryString = [];

    if (opt.lat && opt.lon) {
        queryString.push("lat=" + parseFloat(opt.lat));
        queryString.push("lon=" + parseFloat(opt.lon));
    } else if (opt.city || opt.zip) {
        var subQuerystring = [];
        if (opt.city) {
            subQuerystring.push(opt.city);
        } else if (opt.zip) {
            subQuerystring.push("zip=" + opt.zip);
        }

        if (opt.iso) {
            subQuerystring.push(opt.iso);
        }

        queryString.push(subQuerystring.join(","));
    } else if (opt.id) {
        queryString.push('id=' + parseInt(opt.id));
    }

    if (!queryString.length) return;

    queryString.push("APPID=" + this.defaults.key);

    var path = this.defaults.base_path + "/weather?" + queryString.join("&");
    
    processRequest(this.defaults.base_url, path, function response(data) {
        var json = getParsedJSON(data);
        // handle JSON response
        callback(json);
    });
}

/**
 *  Get relevant bits of information that we care about from the Weather API JSON response
 *  @param {string} rawJSON - The raw string representation of the JSON response
 *  @returns {object} - The JSON containing the relevant info about the weather
 */
function getParsedJSON(rawJSON) {
    var json = JSON.parse(rawJSON);
    if (!json.main) {
        return {
            Error: json.message
        };
    }

    var weatherKelvin = json.main.temp;
    var weatherCelsius = weatherKelvin - 273.15;
    var weatherFahrenheit = (weatherKelvin - 273.15) * 1.8000 + 32.00;

    return {
        city_id: json.id,
        city: json.name,
        temp: json.main.temp,
        temp_f: weatherFahrenheit,
        temp_c: weatherCelsius,
        weather_id: json.weather.id,
        main: json.weather.main,
        description: json.weather.description
    };
}

/**
 *  Make a request to the third party client
 *  @param {string} path - The API path we wish to access
 *  @param {function} responseCallback - The function to run for a successfully request
 */
function processRequest(baseURL, path, responseCallback) {
    var opts = {
        hostname: baseURL,
        port: 80,
        path: path,
        headers: {
            'Content-type': 'application/json'
        }
    };

    var req = http.request(opts, (res) => {
        res.setEncoding('utf-8');
        res.on('data', responseCallback);
    });

    req.on('error', e => {
        console.log('Error occured with API request: ' + e.message);
    });

    req.end();
}

// Add any specific methods for the third party weather client, OpenWeatherMap API
exports.WeatherClient = WeatherClient;
