"use strict";

module.exports = function(sequelize, DataTypes) {
  var ArtistFacts = sequelize.define("artist_facts", {
    artistfact_id: {
      type: DataTypes.INTEGER,
      primary_key: true
    },
    artist_id: DataTypes.INTEGER,
    fact_id: DataTypes.INTEGER,
  }, {
    classMethods: {
      associate: function(models) {
        ArtistFacts.belongsTo(models.artists, {
          onDelete: "RESTRICT",
          foreignKey: {
            allowNull: false
          }
        });

        ArtistFacts.belongsTo(models.facts, {
          onDelete: "RESTRICT",
          foreignKey: {
            allowNull: false
          }
        });
      }
    }
  });

  return ArtistFacts;
};
