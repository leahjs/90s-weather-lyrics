"use strict";

module.exports = function(sequelize, DataTypes) {
  var Facts = sequelize.define("facts", {
    fact_id: {
      type: DataTypes.INTEGER,
      primary_key: true
    },
    timestamp: DataTypes.DATE,
    text: DataTypes.TEXT,
    type: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        Facts.hasMany(models.artist_facts);
      }
    }
  });

  return Facts;
};
