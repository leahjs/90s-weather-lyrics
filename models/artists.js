"use strict";

module.exports = function(sequelize, DataTypes) {
  var Artists = sequelize.define("artists", {
    artist_id: {
      type: DataTypes.INTEGER,
      primary_key: true
    },
    first_name: DataTypes.STRING(40),
    last_name: DataTypes.STRING(40),
    moniker: DataTypes.STRING(40),
    full_name: DataTypes.TEXT,
    dob: DataTypes.DATE,
    death: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    classMethods: {
      associate: function(models) {
        Artists.hasMany(models.artist_facts);
      }
    }
  });

  return Artists;
};
