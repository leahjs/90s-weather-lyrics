var express = require('express');                                                                                                                                                                     
var router = express.Router();

var weatherClient = require('../lib/weather-client');

router.get("/", function(req, res) {
    var opts = null;
    if (req.query.lat && req.query.lon) {
        opts = {
            lat: req.query.lat,
            lon: req.query.lon,
        };
    } else if (req.query.zip) {
        opts = {
            zip: req.query.zip
        };
    }

    if (!opts) {
        res.status(400).json({
            error: 'Need the zip code or lat/lon to get the weather.'
        });
        res.end();
    }

    var client = new weatherClient.WeatherClient();
    client.getCurrentWeather(opts, function(json) {
        //TODO: Do proper error handling
        if (json.Error) {
            res.status(400);
        } else {
            res.status(200);
        }
        res.send(json);
        res.end();
    });
});

module.exports = router;
