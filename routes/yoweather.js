var express = require('express');
var router = express.Router();

router.get('/yoweather', function(req, res, next) {
  res.render('yoweather.pug');
});

module.exports = router;
